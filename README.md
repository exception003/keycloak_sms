# Keycloak_SMS

Basic keycloak container with added sms modules.

# Getting started

Before run container edit ENVs in Dockerfile:
```
ENV KEYCLOAK_ADMIN=
ENV KEYCLOAK_ADMIN_PASSWORD=
ENV KC_DB_URL=jdbc:mysql://mysql-database-container:port/keycloak?ssl=allow
ENV KC_DB_USERNAME=
ENV KC_DB_PASSWORD=
ENV KC_HOSTNAME=
```

# Author
Exception003

# License
GPLv3
